# Graphical Node Editor

This is a graphical tool for visualizing and editing directed acyclic graphs.
A graph is defined by different nodes, each consisting of

- inputs
- outpus and
- options.

A nodes' output can be connected to the inputs of other nodes, so that a flow
of information can be established. Typed values enable compatibility checking between inputs and outputs.

## Example
See this page on `Some online html/css/js website` for an interactive example.

## Building

First install all deps:

```
npm install
```

To build the lib:
```
gulp
```

To view an example, run:
```
gulp examples
```
and then open `examples/index.html`.




## Node Types

## Serialization

TODO

## License
This software is distributed under the BSD 3-Clause license. See `LICENSE.txt`.