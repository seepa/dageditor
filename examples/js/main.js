/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict'

let interact = require('interact.js');
import {Add, Value} from 'lib/tests/test_node_types.js';
import {Images} from 'examples/node_types/images.js';
import {Makescene} from 'examples/node_types/makescene.js';
import {Sfmrecon} from 'examples/node_types/sfmrecon.js'
import {Dmrecon} from 'examples/node_types/dmrecon.js';
import {Scene2pset} from 'examples/node_types/scene2pset.js';
import {Fssrecon} from 'examples/node_types/fssrecon.js';
import {Meshclean} from 'examples/node_types/meshclean.js';
import {Pipeline, registerNodeType, pipelineFromJson, serializeNodeTypes} from 'pipeline/pip.js';
import {Editor} from 'lib/editor.js';

var svgCanvas = document.getElementById('workspace');
let pipelineJson =
[
    {
        "type": "Images",
        "id": "images1"
    },
    {
        "type": "Makescene",
        "id": "makescene1",
        "options": {
            "m": "24000000"
        },
        "inputs": {
            "images-path": {
                "id": "images1",
                "output": "images-path"
            }
        }
    },
    {
        "type": "Sfmrecon",
        "id": "sfm1",
        "options" : {
            "m" : "240000",
            "initial-pair": "1,2"
        },
        "inputs": {
            "scene-path": {
                "id": "makescene1",
                "output": "scene-path"
            }
        }
    },
    {
        "type": "Dmrecon",
        "id": "dm1",
        "options" : {
            "s" : "2"
        },
        "inputs": {
            "scene-path": {
                "id": "sfm1",
                "output": "scene-path"
            }
        }
    },
    {
        "type": "Dmrecon",
        "id": "dm2",
        "options" : {
            "s" : "3"
        },
        "inputs": {
            "scene-path": {
                "id": "sfm1",
                "output": "scene-path"
            }
        }
    },
    {
        "type": "Scene2pset",
        "id": "s2p1",
        "options" : {
            "F" : "2"
        },
        "inputs": {
            "scene-path": {
                "id": "dm1",
                "output": "scene-path"
            }
        }
    },
    {
        "type": "Scene2pset",
        "id": "s2p2",
        "options" : {
            "F" : "3"
        },
        "inputs": {
            "scene-path": {
                "id": "dm2",
                "output": "scene-path"
            }
        }
    },
    {
        "type": "Fssrecon",
        "id": "fssr1",
        "inputs": {
            "ply-path-0": {
                "id": "s2p1",
                "output": "ply-path"
            },
            "ply-path-1": {
                "id": "s2p2",
                "output": "ply-path"
            }
        }
    },
    {
    "type" : "Meshclean",
    "id" : "mclean",
    "inputs" : {
        "ply-path": {
            "id": "fssr1",
            "output": "ply-path"
        }
    },
    "options": {
        "t": "0.5",
        "c": "2000"
    }
}
];
let layout = [{"images1":{"x":0.079153,"y":0.149369}},{"makescene1":{"x":0.14116566666666666,"y":0.234153}},{"sfm1":{"x":0.21986166666666668,"y":0.23627}},{"dm1":{"x":0.3082206666666667,"y":0.13147499999999998}},{"dm2":{"x":0.30693333333333334,"y":0.37094900000000003}},{"s2p1":{"x":0.39703666666666665,"y":0.197865}},{"s2p2":{"x":0.39783999999999997,"y":0.413003}},{"fssr1":{"x":0.48864,"y":0.341337}},{"mclean":{"x":0.56533,"y":0.27429899999999996}}];

registerNodeType(Value)
registerNodeType(Add)
registerNodeType(Sfmrecon)
registerNodeType(Images)
registerNodeType(Makescene)
registerNodeType(Dmrecon)
registerNodeType(Scene2pset)
registerNodeType(Fssrecon)
registerNodeType(Meshclean)
let pipeline = pipelineFromJson(pipelineJson);

let editor = new Editor({
    pipeline: pipeline,
    layout: layout,
    viewportHeight: 800
});

// add list elements for each available node type
let ul = document.getElementById('node-list');
let nodeTypes = serializeNodeTypes();
// sort them lexicographically
nodeTypes.sort(function (a, b) {
    return a.type.toLowerCase() > b.type.toLowerCase()
});
for (let i = 0; i < nodeTypes.length; ++i) {
    let li = document.createElement('li');
    let a = document.createElement('a');
    a.setAttribute('href', '#');
    a.setAttribute('class', 'add-node');
    a.setAttribute('name', nodeTypes[i].type);
    a.innerHTML = nodeTypes[i].type;
    li.appendChild(a)
    ul.appendChild(li)
}

// dispatch an 'add-node' event when the user clicks on a dom element with
// the class 'add-node'
let elements = document.querySelectorAll('.add-node')
for (let i = 0; i < elements.length; ++i) {
    let element = elements[i];
    let addEvent = new CustomEvent('add-node', {
        'detail': {
            'type': element.getAttribute('name')
        }
    });
    element.addEventListener('click', function(event) {
        event.preventDefault();
        editor.workspace.dispatchEvent(addEvent);
    });
}

//
// Tests
//
// test dag
// import {testDag} from './tests/test_dag.js';
// testDag();

// test reconstruction pipeline
// import {testSerialization1, testSerialization2} from './tests/test_serialization.js';
// testSerialization2();




// let pipelineJson =
// [
//     {
//         "type": "Value",
//         "id": "v1",
//         "options": {
//             "value": 5
//         }
//     },
//     {
//         "type": "Value",
//         "id": "v2",
//         "options": {
//             "value": 2
//         }
//     },
//     {
//         "type": "Add",
//         "id": "add1",
//         "inputs": {
//             "v1": {
//                 "id": "v1",
//                 "output": "v"
//             },
//             "v2": {
//                 "id": "v2",
//                 "output": "v"
//             }
//         }
//     },
//     {
//         "type": "Add",
//         "id": "add2",
//         "inputs": {
//             "v1": {
//                 "id": "add1",
//                 "output": "v"
//             }
//         },
//         "options": {
//             "multiplier": 3
//         }
//     }
// ];
