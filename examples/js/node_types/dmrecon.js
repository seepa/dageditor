/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {MVEScenePath, Number} from 'dag/typedvalue.js';

export class Dmrecon extends Node {
    constructor({id} = {}) {
        super({id});
        this._inputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._outputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._options = {
            'max-pixels' : new Number({title: 'Max. pixels', defaultValue: 6000000}),
            's' : new Number({title: 'Scale', description:'Reconstruction on given scale, 0 is original', required: false}),
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Dmrecon';
    }
    compute() {
        this._outputs['scene-path'].value = this._inputs['scene-path'].value;
    }
}
