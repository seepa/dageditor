/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {MVEScenePath, Number, PLYPath} from 'dag/typedvalue.js';

export class Fssrecon extends Node {
    constructor({id} = {}) {
        super({id});
        this._inputs = {
            'ply-path-0' : new PLYPath({title: 'Pset 0'}),
            'ply-path-1' : new PLYPath({title: 'Pset 1'})
        };
        this._outputs = {
            'ply-path' : new PLYPath({title: 'Surface'})
        };
        this._options = {
            // model this as a dropdown menu...
            // --interpolation=ARG Interpolation: linear, scaling, lsderiv, [cubic]
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Fssrecon';
    }
    compute() {
        this._outputs['ply-path'].value = './fssr-surface.ply';
    }
}
