/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {Path} from 'dag/typedvalue.js';

export class Images extends Node {
    constructor({value = undefined, id} = {}) {
        super({id});
        this._outputs = {
            'images-path' : new Path({title: 'Images Path'})
        };
        this.init()
    }
    static get type() {
        return 'Images';
    }
    compute() {
        this._outputs['images-path'].value = "./images";
    }
}
