/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {MVEScenePath, Path, Number} from 'dag/typedvalue.js';

export class Makescene extends Node {
    constructor({value = undefined, id} = {}) {
        super({id});
        this._inputs = {
            'images-path': new Path({title: 'Input Images'})
        };
        this._outputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._options = {
            'm' : new Number({title: 'Max. pixels', defaultValue: 6000000})
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Makescene';
    }
    compute() {
        this._outputs['scene-path'].value = "./scene";
    }
}
