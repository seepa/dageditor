/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {PLYPath, Number} from 'dag/typedvalue.js';

export class Meshclean extends Node {
    constructor({id} = {}) {
        super({id});
        this._inputs = {
            'ply-path' : new PLYPath({title: 'Ply'})
        };
        this._outputs = {
            'ply-path' : new PLYPath({title: 'Cleaned Ply'})
        };
        this._options = {
            't': new Number({title: 'Confidence Threshold', defaultValue: 1, min: 0,
                description: 'Threshold on the geometry confidence'}),
            'p': new Number({title: 'Confidence Percentile', required: false,
                description:'Use the nth percentile (0 - 100) as confidence threshold'}),
            'c': new Number({title: 'Component size', defaultValue: 1000,
                description: 'Minimum number of vertices per component'})
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Meshclean';
    }
    compute() {
        let path = this._inputs['ply-path'].value
        let fullname = path.replace(/^.*[\\\\/]/, '');
        let name = fullname.split('.')[0];
        let suffix = fullname.split('.')[1];
        this._outputs['ply-path'].value = `${name}-clean.${suffix}`;
    }
}
