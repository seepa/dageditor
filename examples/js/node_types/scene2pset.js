/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {MVEScenePath, PLYPath, Number} from 'dag/typedvalue.js';

export class Scene2pset extends Node {
    constructor({id} = {}) {
        super({id});
        this._inputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._outputs = {
            'ply-path' : new PLYPath({title: 'Ply File'})
        };
        this._options = {
            'F' : new Number({title: 'Fssr output', description:'FSSR output, sets -nsc and -di with scale ARG', defaultValue: 0})
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Scene2pset';
    }
    compute() {
        this._outputs['ply-path'].value = `./pset-L${this.options['F'].value}`;
    }
}
