/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {MVEScenePath, Number, String, Boolean} from 'dag/typedvalue.js';

export class Sfmrecon extends Node {
    constructor({id} = {}) {
        super({id});
        this._inputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._outputs = {
            'scene-path' : new MVEScenePath({title: 'Scene'})
        };
        this._options = {
            'm' : new Number({title: 'Max. pixels', defaultValue: 6000000}),
            'initial-pair': new String({title:'Initial Pair', defaultValue: '-1,-1',
            description: 'Manually specify initial pair IDs.'}),
            'cascade-hashing': new Boolean({title: 'Use Cascade-Hashing',
            defaultValue: true, description: 'Use Cascade-Hashing (faster than brute-force hashing).'})
        };
        this.init()
        console.log('Created node ', this);
    }
    static get type() {
        return 'Sfmrecon';
    }
    compute() {
        this._outputs['scene-path'].value = this._inputs['scene-path'].value;
    }
}
