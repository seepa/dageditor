'use strict';

var lib = './lib/js';
var examples = './examples/js';

module.exports = {
    lib: lib,
    main: 'index.js',
    dist: 'dist',
    styles: {
        src: './lib/scss/**/*.scss',
        dst: './dist',
        sassIncludePaths: ['./node_modules/bootstrap-sass/assets/stylesheets/']
    },
    examples:{
        dist: './examples/dist',
        js: {
            src: './examples/js/main.js',
            dst: './examples/dist'
        },
        styles: {
            src: './examples/scss/**/*.scss',
            dst: './examples/dist',
            sassIncludePaths: ['./node_modules/bootstrap-sass/assets/stylesheets/']
        }
    },
    watch: {
    paths: ['js'].reduce(function(paths, ext) {
        return paths.concat([lib + '/**/*.' + ext, lib + '/*.' + ext, examples + '/**/*.' + ext, examples + '/*.' + ext]);
        }, [])
    }
};
