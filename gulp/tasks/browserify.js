var gulp = require('gulp');
var gulpif = require('gulp-if');
var browserify = require('browserify');
var babelify = require('babelify');
var config = require('../config');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var aliasify = require('aliasify');
var derequire = require('gulp-derequire');
var sourcemaps = require('gulp-sourcemaps');

/**
 * Strips the directory from the path.
 *
 * @param {object} path
 */
function stripDirectory(path) {
  path.dirname = '';
}

gulp.task('browserify', function() {
    const createSourcemap = !global.isProd;
    var browserifyOpts = {
        entries: ['./' + config.main],
        standalone: 'dageditor'
    }
    return browserify(browserifyOpts)
        .transform(babelify, {presets: ["es2015"]})
        .transform(aliasify)
        .bundle()
        .pipe(source(config.main))
        .pipe(gulpif(createSourcemap, sourcemaps.init()))
        .pipe(derequire())
        .pipe(buffer())
        .pipe(rename(stripDirectory))
        .pipe(gulp.dest(config.dist))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulpif(
            createSourcemap,
            sourcemaps.write(global.isProd ? '.' : null ))
        )
        .pipe(gulp.dest(config.dist));
});
