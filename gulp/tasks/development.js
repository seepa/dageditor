'use strict';

var config = require('../config');
var gulp = require('gulp');

gulp.task('dev', ['examples'], function() {
    global.isProd = false;
    return gulp.watch(config.watch.paths, ['examples']);
});
