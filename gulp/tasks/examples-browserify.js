'use strict';

var config = require('../config');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var aliasify = require('aliasify');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('examples-browserify', function() {
    const createSourcemap = !global.isProd;
    var browserifyOpts = {
        entries: [config.examples.js.src]
    }
    return browserify(browserifyOpts)
        .transform(babelify, {presets: ["es2015"]})
        .transform(aliasify)
        .bundle()
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(gulpif(createSourcemap, sourcemaps.init()))
        .pipe(buffer())
        .pipe(gulpif(
            createSourcemap,
            sourcemaps.write(global.isProd ? '.' : null))
        )
        .pipe(gulp.dest(config.examples.js.dst));
});
