'use strict';

var config = require('../config');
var gulp = require('gulp');
var del = require('del');

gulp.task('examples-clean', function (cb) {
    return del([config.examples.dist], cb);
});
