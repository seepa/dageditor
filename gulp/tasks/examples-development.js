'use strict';

var config = require('../config');
var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('examples', ['build', 'examples-clean'], function(cb) {
  global.isProd = false;
  runSequence(['examples-browserify', 'examples-styles'], cb);
});
