/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import * as helpers from 'lib/helpers.js';
import {Boolean} from 'dag/typedvalue.js';

export class Component {
    constructor(x, y) {
        // 2d position (relative to parent)
        this._x = x;
        this._y = y;
        // the dom element
        this._element = undefined;
        // children of type component
        this.children = [];
    }

    draw() {
        for (var i = 0; i < this.children.length; ++i) {
            this.children[i].draw();
        }
    }

    appendChild(child) {
        this._element.appendChild(child.element)
        this.children.push(child)
    }
    get element() {
        return this._element;
    }
    set element(elem) {
        this._element = elem;
    }
    get width() {
        return this.element.offsetWidth;
    }
    get height() {
        return this.element.offsetHeight;
    }
    set x(n) {this._x = n;}
    set y(n) {this._y = n;}
    get x() {return this._x;}
    get y() {return this._y;}
}

export class CTextBox extends Component {
    constructor(x, y, text) {
        super(x, y);
        this.text = text;
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cTextBox');
        let span = document.createElement('span');
        span.innerHTML = text;
        this.element.appendChild(span);
    }

    draw() {
        // draw the rect
        this.element.children[0].setAttribute('position', 'relative');
        this.element.children[0].setAttribute('left', this._x);
        this.element.children[0].setAttribute('top', this._y);
    }
}

class CDropDown extends Component {
    constructor(x, y, node, workspace) {
        super(x,y);
        this._workspace = workspace;
        this._node = node;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'btn-group cNode-dropdown');

        let btn = document.createElement('button');
        btn.setAttribute('class', 'btn btn-icon-bar dropdown-toggle');
        btn.setAttribute('data-toggle', 'dropdown');
        btn.setAttribute('aria-haspopup', 'true');
        btn.setAttribute('aria-expanded', 'false');

        // place icon bars inside the button
        btn.innerHTML = '<span class="icon-bars-button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></span>';

        this.element.appendChild(btn);

        let ul = document.createElement('ul');
        ul.setAttribute('class', 'dropdown-menu pull-right');

        let li1 = document.createElement('li');
        let a1 = document.createElement('a');
        a1.innerHTML = 'Delete';

        let deleteEvent = new CustomEvent('delete-node', {
            'detail': {
                'id': this._node.id
            }
        });
        let that = this;
        a1.addEventListener('click', function(event) {
            event.preventDefault();
            that._workspace.dispatchEvent(deleteEvent);
        });

        li1.appendChild(a1);
        ul.appendChild(li1);
        this.element.appendChild(ul);
    }
}

class CHeader extends Component {
    constructor(x, y, node, workspace) {
        super(x, y);
        this._node = node;
        this._workspace = workspace;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cHeader clearfix');

        let cTitle = new CTextBox(0, 0, this._node.constructor.type);
        cTitle.element.setAttribute('class', 'nodeTitle');
        this.appendChild(cTitle);

        // create dropdown
        let dropdown = new CDropDown(0, 0, this._node, this._workspace);
        this.appendChild(dropdown);
    }
}

export class CInput extends Component {
    constructor(x, y, input) {
        super(x, y);
        this._input = input;
        this._title = this._input.title;
        this._type = this._input.type;
        this.createComponents();
    }

    get input() {
        return this._input;
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cInput');
        let span1 = document.createElement('span');
        span1.innerHTML = `${this._title}`;
        let span2 = document.createElement('span');
        span2.setAttribute('class', 'typedValue');
        span2.innerHTML =  ` (${this._type})`;

        let inputConn = document.createElement('div');
        inputConn.className += 'connector input-connector connector-default';
        inputConn.setAttribute('name', this._input.name);

        this.element.appendChild(span1);
        this.element.appendChild(span2);
        this.element.appendChild(inputConn);
    }
}

export class COutput extends Component {
    constructor(x, y, output) {
        super(x, y);
        this._output = output;
        this._title = this._output.title;
        this._type = this._output.type;
        this.createComponents();
    }

    get output() {
        return this._output;
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cOutput');
        let span1 = document.createElement('span');
        span1.innerHTML = `${this._title}`;
        let span2 = document.createElement('span');
        span2.setAttribute('class', 'typedValue');
        span2.innerHTML =  ` (${this._type})`;

        let outputConn = document.createElement('div');
        outputConn.className += 'connector output-connector connector-default';
        outputConn.setAttribute('name', this._output.name);
        this.element.appendChild(span1);
        this.element.appendChild(span2);
        this.element.appendChild(outputConn);
    }
}

export class COption extends Component {
    constructor(x, y, option) {
        super(x, y);
        this._option = option;
        this._title = this._option.title;
        this._value = this._option.value;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cOption');
        let name = document.createElement('span');
        let valueInput = document.createElement('input');
        name.setAttribute('class', 'optionName');
        valueInput.setAttribute('class', 'optionValue');
        name.innerHTML = `${this._title}: `;

        if (this._value == undefined) {
            valueInput.value = '';
        }else {
            valueInput.value = `${this._value}`;
        }

        let that = this;
        valueInput.addEventListener('input', function() {
            that._option.value = this.value;
            console.log(`Changed input ${that._title} to ${this.value}`);
        });

        this.element.appendChild(name);
        this.element.appendChild(valueInput);
    }
}

export class COptionBoolean extends Component {
    constructor(x, y, option) {
        super(x, y);
        this._option = option;
        this._title = this._option.title;
        this._value = this._option.value;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cOptionBoolean');
        let label = document.createElement('label');
        let valueInput = document.createElement('input');
        valueInput.setAttribute('type','checkbox');
        label.setAttribute('class', 'optionName');
        valueInput.setAttribute('class', 'optionBooleanValue');
        label.innerHTML = `${this._title}`;

        if (this._value == undefined) {
            valueInput.value = false;
            valueInput.checked = false;
        }else {
            valueInput.value = this._value;
            valueInput.checked = this._value;
        }

        let that = this;
        valueInput.addEventListener('change', function() {
            that._option.value = this.checked;
            // console.log(`Changed COptionBoolean "${that._title}" to ${this.checked}`);
        });

        label.appendChild(valueInput);
        this.element.appendChild(label);
    }
}

export class CInputContainer extends Component {
    constructor(x, y, inputs) {
        super(x, y);
        this._inputs = inputs;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cInputContainer');
        for (let key in this._inputs) {
            let input = this._inputs[key];
            // console.log('Adding Input: ', input);
            let cInput = new CInput(0, 0, input);
            this.appendChild(cInput);
        }
    }

    getConnectorPos(inputName) {
        // console.log('Get input pos for node', this._node, ' inputkey: ', inputName);
        for (let child of this.children) {
            if (child.constructor.name !== 'CInput')
                continue;
            let name = child.input.name;
            if (name !== inputName)
                continue;

            let connector = helpers.getChildByClass(child.element, 'connector');
            return {
                'x': this._x + child.element.offsetLeft - (connector.offsetWidth/2) + 2,
                'y': this._y + child.element.offsetTop + (child.element.offsetHeight / 2)
            }
        }
        return {'x': this._x, 'y': this._y};
    }
}

export class COutputContainer extends Component {
    constructor(x, y, outputs) {
        super(x, y);
        this._outputs = outputs;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cOutputContainer');
        for (let key in this._outputs) {
            let elem = this._outputs[key];
            let cOutput = new COutput(0, 0, elem);
            this.appendChild(cOutput);
        }
    }

    getConnectorPos(outputName) {
        for (let child of this.children) {
            if (child.constructor.name !== 'COutput')
                continue;
            let name = child.output.name;
            if (name !== outputName)
                continue;

            let connector = helpers.getChildByClass(child.element, 'connector');
            return {
                'x': this._x + child.element.offsetLeft + child.element.offsetWidth + (connector.offsetWidth/2) - 2,
                'y': this._y + child.element.offsetTop + (child.element.offsetHeight / 2)
            }
        }
        return {'x': this._x, 'y': this._y};
    }
}

export class COptionContainer extends Component {
    constructor(x, y, options) {
        super(x, y);
        this._options = options;
        this.createComponents();
    }

    createComponents() {
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cOptionContainer');
        for (let key in this._options) {
            let elem = this._options[key];
            let OptionClass = COption;
            // create specific option elements for certain option types
            if (elem instanceof Boolean) {
                OptionClass = COptionBoolean;
            }
            let cOption = new OptionClass(0, 0, elem);
            this.appendChild(cOption);
        }
    }
}

export class CNode extends Component {
    constructor(x, y, node, workspace) {
        super(x, y);
        // save original unscaled (x,y) position (needed for proper zooming)
        this._unscaledX = x;
        this._unscaledY = y;
        this._node = node;
        this.element = document.createElement('div');
        this.element.setAttribute('class', 'cNode');
        this.element.setAttribute('data-index', node.id);
        this._id = node.id;
        // this._type = node.constructor.type;
        this._workspace = workspace;
        this.createComponents();
    }

    createComponents() {
        console.log('Creating components for ', this._id);

        // create header
        let cHeader = new CHeader(0, 0, this._node, this._workspace);
        this.appendChild(cHeader);

        // add inputs
        if (!helpers.isEmptyObject(this._node.inputs)) {
            this._inputContainer = new CInputContainer(0, 0, this._node.inputs);
            this.appendChild(this._inputContainer);
        }

        // add options
        if (!helpers.isEmptyObject(this._node.options)) {
            this._optionContainer = new COptionContainer(0, 0, this._node.options);
            this.appendChild(this._optionContainer);
        }

        // add outputs
        if (!helpers.isEmptyObject(this._node.outputs)) {
            this._outputContainer = new COutputContainer(0, 0, this._node.outputs);
            this.appendChild(this._outputContainer);
        }
    }

    draw() {
        super.draw();
        // translate the element
        let matrix = [1,0,0,1, this._unscaledX, this._unscaledY];
        this.element.style.webkitTransform =
        this.element.style.transform = helpers.createTransformMatrixStr(matrix);
    }

    getInputPos(name) {
        if (name == undefined)
            throw 'Error: Name of output is undefined';
        let pos = this._inputContainer.getConnectorPos(name);
        let matrix = helpers.parseTransformMatrix(this.element.style.transform);
        pos.x *= matrix[0];
        pos.y *= matrix[3];
        pos.x += this._x;
        pos.y += this._y;
        return pos;
    }
    getOutputPos(name) {
        if (name == undefined)
            throw 'Error: Name of output is undefined';
        let pos = this._outputContainer.getConnectorPos(name);
        let matrix = helpers.parseTransformMatrix(this.element.style.transform);
        pos.x *= matrix[0];
        pos.y *= matrix[3];
        pos.x += this._x;
        pos.y += this._y;
        return pos;
    }
}
