/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

export class Output {
    constructor({node, type, title = 'Output'} = {}) {
        this._type = type;
        this._title = title;
        this._node = node;
        this._valid = false;
        this._computationRunning = false;
    }

    _ensure_value() {
        if (this._valid) return;
        if (this._computationRunning)
            throw 'Error: Cycle in node "' + this._node.id + '" detected';

        this._computationRunning = true;
        this._node.compute();
        this._computationRunning = false;
    }

    get value() {
        this._ensure_value();
        return this._value;
    }
    set value(v) {
        this._value = v;
        this._valid = true;
    }
    get type() {
        return this._type;
    }

    get title() {
        return this._title;
    }

    get node() {
        return this._node;
    }
    // find the name of this output
    get name() {
        let outputKey = undefined;
        Object.keys(this._node._outputs).forEach(function(key, index) {
            if (this == this._node._outputs[key]) {
                outputKey = key;
            }
        }, this);
        if (outputKey == undefined)
            throw 'Error: Cannot find output key in owining node!';
        return outputKey;
    }

    toJson() {
        return {'type': this.type}
    }
}

export class Input {
    constructor(key, typedValue) {
        this._typedValue = typedValue;
        this._source = undefined;
        // the name is the key responding to this input
        this._name = key;
    }

    get value() {
        let outputValue = undefined;
        if (this._source !== undefined) {
            outputValue = this._source.value;
        }
        this._typedValue.value = outputValue;
        return this._typedValue.value;
    }
    get type() {
        return this._typedValue.type;
    }
    get title() {
        return this._typedValue.title;
    }
    get name() {
        return this._name;
    }
    get source() {
        return this._source;
    }
    set source(src) {
        if (src != undefined && this._typedValue.type !== src.type) {
            throw 'Error: Input/Output type mismatch! Input has type "' + this._typedValue.type + '" but the output to be connected has type "' + src.type + '"';
        }
        this._source = src;
    }

    toJson() {
        return this._typedValue.toJson();
    }
}

export class Node {
    constructor({id} = {}) {
        this._id = id;
        this._inputs = {};
        this._outputs = {};
        this._options = {};
    }

    init() {
        // if (this._inputs == undefined) this._inputs = {};
        // if (this._outputs == undefined) this._outputs = {};
        // if (this._options == undefined) this._options = {};

        for (let key in this._inputs)
            this._inputs[key] = new Input(key, this._inputs[key]);

        for (let key in this._outputs) {
            this._outputs[key] = new Output({
                node: this,
                type: this._outputs[key].type,
                title: this._outputs[key].title
            });
         }
    }

    get options() {
        return this._options;
    }
    get inputs() {
        return this._inputs;
    }
    get outputs() {
        return this._outputs;
    }

    get id() {
        return this._id;
    }

    static get type() {
        throw 'Error: Each subclass must override the this method';
    }
}
