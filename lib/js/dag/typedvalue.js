/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';


export class TypedValue {
    constructor({title, description, defaultValue, required = false} = {}) {
        if (defaultValue != undefined && required) {
            throw 'Error: cannot set both required and default.';
        }
        if (required == undefined) {
            required = (defaultValue == undefined);
        }
        this._required = required;
        this._title = title;
        this._description = description;
        this._default = defaultValue;
    }

    get value() {
        if (this._required && this._value == undefined) {
            throw 'Required option not set!';
        }
        if (this._value == undefined) {
            return this._default;
        }
        return this._value;
    }

    set value(v) {
        if (v == undefined) {
            if (this._required) {
                throw 'Required value is none';
            } else {
                this._value = undefined;
            }
        } else {
            this._value = this._parse_input(v);
        }
    }

    get type() {
        return this.constructor.name;
    }

    get title() {
        return this._title;
    }

    // should be overriden by subclasses
    _parse_input(input) {
        return input
    }

    toJson() {
        return {
            'type': this.type,
            'required': this._required,
            'default': this._default,
            'description': this._description
        };
    }
}

export class Boolean extends TypedValue {
    _parse_input(input) {
        if (input == false || input == undefined) {
            return false;
        } else {
            return true;
        }
    }
}

export class Number extends TypedValue {
    constructor(obj) {
        // console.log('Object is:', obj);
        super(obj);
        if (obj != undefined) {
            this._min = obj['min'];
            this._max = obj['max'];
        }
    }
    _parse_input(input) {
        if ((this._min != undefined && input < this._min)
            || (this._max != undefined && input > this._max)) {
                console.log('Error: Number' + number + ' out of range!');
        }
        else {
            return input
        }
    }

    toJson() {
        let obj = super.toJson()
        obj['min'] = this._min;
        obj['max'] = this._max;
        return obj;
    }
}

export class String extends TypedValue {
    _parse_input(input) {
        try {
            if (typeof(input) !== 'string') {
                throw 'Error: Input not a string!';
            }
            if (input.length < 1) {
                throw 'Error: Input string too short!';
            }
        }
        catch (err) {
            console.log('Error parsing input: ' + err);
        }
        return input;
    }
}

export class Path extends String {
    constructor(obj) {
        super(obj);
    }
}
export class MVEScenePath extends Path {
    constructor(obj) {
        super(obj);
    }
}
export class PLYPath extends Path {}
