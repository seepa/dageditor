/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import './import_jquery.js'
import 'bootstrap/js/dropdown.js';

import {CNode} from 'lib/component.js';
import * as helpers from 'lib/helpers.js';
import {getNodeType} from 'pipeline/pip.js';

let interact = require('interact.js');


// Editor for displaying/editing an DAG (Directed Acyclic Graph)
export class Editor {
    constructor({pipeline=undefined, workspaceId='workspace',
        workspaceViewportId='workspace-viewport', canvasId='canvas-overlay',
        workspaceWidth=3000, workspaceHeight=1000, viewportWidth=undefined,
        viewportHeight=500, hiddenWorkspaceLeft=100, hiddenWorkspaceTop=100,
        layout=undefined}={}) {

        // keep a reference to this
        let that = this;

        // get the workspace viewport element
        this._workspaceViewport = document.getElementById(workspaceViewportId);
        // viewport{Width,Height} define the window in px through which
        // the workspace can be seen/manipulated
        this._viewportWidth = viewportWidth;
        this._viewportHeight = viewportHeight;
        if (viewportWidth !== undefined) {
            this._workspaceViewport.style.width = `${viewportWidth}px`;
        }
        if (viewportHeight !== undefined) {
            this._workspaceViewport.style.height = `${viewportHeight}px`;
        }

        // hiddenWorkspace{Top,Left} define the amount of pixel of the workspace
        // that will be hidden to the top/left by default.
        // The workspace is translated by this amount to the top/left
        this._hiddenWorkspaceLeft = hiddenWorkspaceLeft;
        this._hiddenWorkspaceTop = hiddenWorkspaceTop;

        // workspace{Width,Height} define the size in px of the workspace
        this._workspaceWidth = workspaceWidth;
        this._workspaceHeight = workspaceHeight;
        // resize the workspace and set its css transform matrix
        this._workspace = document.getElementById(workspaceId);
        this._workspace.style.width = `${this._workspaceWidth}px`;
        this._workspace.style.height = `${this._workspaceHeight}px`;
        let matrix = [1,0,0,1,0,0];
        this._workspace.style.webkitTransform =
        this._workspace.style.transform = helpers.createTransformMatrixStr(matrix);

        // create a html canvas overlay which is exaclty the size of the workspace
        this._canvas = document.getElementById(canvasId);
        this._canvas.setAttribute('width', this._workspace.offsetWidth);
        this._canvas.setAttribute('height', this._workspace.offsetHeight);
        this._canvasTransform = [1,0,0,1,0,0];
        this._pipeline = pipeline;

        // these will be used when a node connection is dragged by the user
        this._dragStart = undefined;
        this._dragChange = undefined;
        this._dragOutput = undefined;

        // keep track of current left/top translation of the workspace/canvas
        this._leftTranslation = 0;
        this._topTranslation = 0;

        // update tranlation of workspace/canvas
        this.updateTranslation(-this._hiddenWorkspaceLeft, -this._hiddenWorkspaceTop);

        // The zoom step which will be applied when zooming in/out
        this._zoomStep = 0.1;
        // zoom number:
        // for zooming in: increased by 1
        // for zooming out: descreased by 1
        this._zoomNumber = 0;

        // map nodes in the pipeline to component-nodes in the visualization
        this._nodeMap = new Map();
        console.log('Editor.pipeline ', this._pipeline);
        this.drawNodes(layout);
        this.drawConnections();

        // define interact.js interactions on the workspace
        // panning
        interact(`#${workspaceId}`)
          .draggable({
            inertia: true,
            onmove: function(event) {
                let elem = event.target;
                if (event.dx != 0) {
                    that.updateTranslation(event.dx, 0);
                }
                if (event.dy != 0) {
                    that.updateTranslation(0, event.dy);
                }
            }
        });
        // zooming
        interact('#btn-zoom-in').on('tap', function (event) {
            that.zoomIn();
            event.preventDefault();
        });

        interact('#btn-zoom-out').on('tap', function (event) {
            that.zoomOut();
            event.preventDefault();
        });

        interact('#btn-zoom-reset').on('tap', function (event) {
            that.resetZoom();
            event.preventDefault();
        });

        interact('#btn-save').on('tap', function (event) {
            console.log(JSON.stringify(that._pipeline.toJson()));
            console.log(JSON.stringify(that.serializeLayout()));
            event.preventDefault();
        });


        // define the interactions on output/input connectors of each node
        interact('.input-connector').draggable({
            draggable: true,
            onstart: function (event) {
                let target = event.target;
                let inputName = target.getAttribute('name');
                let nodeId = helpers.getParentByClass(target, 'cNode').getAttribute('data-index');
                let node = that._pipeline.nodeMap.get(nodeId);

                // ignore this event, if the input has no ouput connected to it
                let output = node.inputs[inputName].source;
                if (output == undefined) {
                    return;
                }

                // get position of the output
                let cOutputNode = that._nodeMap.get(output.node.id);
                let posOutput = cOutputNode.getOutputPos(output.name);
                let cInputNode = that._nodeMap.get(nodeId);
                let posInput = cInputNode.getInputPos(inputName);

                // unset current input source
                node.inputs[inputName].source = undefined;

                // set info
                that._dragStart = posOutput;
                that._dragChange = {
                    'x': posInput.x - posOutput.x,
                    'y': posInput.y - posOutput.y
                };
                that._dragOutput = output;
            },
            onmove: function (event) {
                if (that._dragOutput == undefined)
                    return;
                that.handleMoveEvent(event);
            },
            onend: function (event) {
                if (that._dragOutput == undefined)
                    return;
                that.handleDragEndEvent(event);
            }
        });

        interact('.output-connector')
        .draggable({
            draggable: true,
            onstart: function (event) {
                let target = event.target;
                let outputName = target.getAttribute('name');
                let nodeId = helpers.getParentByClass(target, 'cNode').getAttribute('data-index');
                let cNode = that._nodeMap.get(nodeId);
                let pos = cNode.getOutputPos(outputName);
                that._dragStart = pos;
                that._dragChange = {'x': 0, 'y': 0};

                // save ref to actual ouput
                let node = that._pipeline.nodeMap.get(nodeId);
                let output = node.outputs[outputName];
                that._dragOutput = output;
            },
            onmove: function (event) {
                that.handleMoveEvent(event);
            },
            onend: function (event) {
                that.handleDragEndEvent(event);
            }
        });

        // define interactions on the nodes
        interact('.cNode')
          .draggable({
            // enable inertial throwing
            inertia: false,
            // keep the element within the area of it's parent
            restrict: {
              restriction: "parent",
              endOnly: true,
              elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
            },
            onmove: function (event) {
                let target = event.target;

                // update current transform matrix
                let matrix = helpers.parseTransformMatrix(target.style.transform);
                matrix[4] += event.dx;
                matrix[5] += event.dy;

                // apply new transform matrix to the element
                target.style.webkitTransform =
                target.style.transform = helpers.createTransformMatrixStr(matrix);

                // update the posiion attributes
                let name = target.getAttribute('data-index');

                // update the unscaled position
                let node = that._nodeMap.get(name);
                node._unscaledX += event.dx;
                node._unscaledY += event.dy;

                that.setNodePosition(name, matrix[4], matrix[5]);
                that.drawConnections();
            },
            onend: function(event) {
                // Add a one-time event which prevents a click. When dragging
                // a node, the resulting click should not affect the node's
                // content.
                // TODO: this doesn't work reliably with inertia enabled !
                let dragTarget = event.target;
                event.target.addEventListener('click', function h (event) {
                    event.preventDefault();
                    dragTarget.removeEventListener(event.type, h);
                });
            }
        });

        // listen for delete-node events
        this._workspace.addEventListener('delete-node', function(event) {
            console.log('Deleting node with id ', `"${event.detail.id}"`);
            that.handleEventDeleteNode(event.detail.id);
        });

        // listen for add-node events
        this._workspace.addEventListener('add-node', function(event) {
            console.log('Add node of type ', `"${event.detail.type}"`);
            that.handleEventAddNode(event.detail.type);
        });
        // TODO: listen for duplicate-node events
    }

    // update the workspace/canvas translation given x,y steps. The transform
    // matrices will be updated
    updateTranslation(stepX, stepY) {
        this._leftTranslation += stepX;
        this._topTranslation += stepY;

        // bound left tranlation
        let maxLeft = Math.abs(this.viewportWidth - this._workspaceWidth);
        this._leftTranslation = Math.min(0, this._leftTranslation);
        this._leftTranslation = Math.max(-maxLeft, this._leftTranslation);

        // bound top translation
        let maxTop = Math.abs(this.viewportHeight - this._workspaceHeight);
        this._topTranslation = Math.min(0, this._topTranslation);
        this._topTranslation = Math.max(-maxTop, this._topTranslation);

        // update workspace
        let matrix = helpers.parseTransformMatrix(this._workspace.style.transform);
        matrix[4] = this._leftTranslation;
        matrix[5] = this._topTranslation;
        this._workspace.style.webkitTransform =
        this._workspace.style.transform = helpers.createTransformMatrixStr(matrix);

        // update canvas
        this._canvas.style.webkitTransform =
        this._canvas.style.transform = helpers.createTransformMatrixStr(matrix);
    }

    // draws an additional arrow from the current output position to the
    // current position of the cursor
    handleMoveEvent (event) {
        let fromX = this._dragStart.x;
        let fromY = this._dragStart.y;
        this._dragChange.x += event.dx;
        this._dragChange.y += event.dy;
        let toX = this._dragStart.x + this._dragChange.x;
        let toY = this._dragStart.y + this._dragChange.y;
        // console.log(`Draw from ${fromX},${fromY} to ${toX},${toY}`);
        this.drawConnections();
        this.drawArrow(fromX, fromY, toX, toY);
    }

    // Checks whether a new connection should be made when the user stops
    // dragging an arrow
    handleDragEndEvent (event) {
        // console.log(`Moved from connector dx ${event.dx} dy ${event.dy}`);
        let pos = {
            'x': this._dragStart.x + this._dragChange.x,
            'y': this._dragStart.y + this._dragChange.y
        };

        // find closest (if any) input to pos
        for (let key of this._pipeline.nodeMap.keys()) {
            let node = this._pipeline.nodeMap.get(key);
            // console.log('Node ', node.id, node.inputs);
            if (helpers.isEmptyObject(node.inputs))
                continue;

            // get the component node
            let cNode = this._nodeMap.get(node.id);

            for (let ikey in node.inputs) {
                let input = node.inputs[ikey];
                // get the position of the input
                let inputPos = cNode.getInputPos(ikey);

                // distance
                let dist = Math.sqrt(Math.pow(inputPos.x - pos.x, 2) +
                    Math.pow(inputPos.y - pos.y, 2));
                if (dist < 20) {
                    // save the new connection
                    try {
                        if (node.id == this._dragOutput.node.id) {
                            throw 'Cannot make a self-connection!';
                        }
                        input.source = this._dragOutput;
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
            }
        }
        this.drawConnections();

        // reset drag helpers
        this._dragStart = undefined;
        this._dragChange = undefined;
        this._dragOutput = undefined;
    }

    // Draws all nodes of the current pipeline.
    // Before drawing, the current canvas (i.e. the dom elements) and the
    // corresponding nodemap are cleared.
    // If a layout is given, the nodes are placed according to the layout
    drawNodes(layout) {
        // clear nodemap and elements from canvas
        for (let key of this._nodeMap.keys()) {
            this._workspace.removeChild(this._nodeMap.get(key).element);
        }
        this._nodeMap.clear();

        // draw nodes
        console.log('Drawing all nodes...');
        let x = Math.abs(this._leftTranslation) + 0.1 * this.viewportWidth;
        let y = Math.abs(this._topTranslation) + 0.1 * this.viewportHeight;
        for (let key of this._pipeline.nodeMap.keys()) {
            let node = this._pipeline.nodeMap.get(key);
            if (layout !== undefined) {
                let pos = this.getPosFromLayout(layout, node.id);
                if (pos !== undefined) {
                    x = pos.x;
                    y = pos.y;
                }
            }
            this.addNodeToCanvas(node, x, y);
        }
    }

    // Creates a CNode holding the given 'node' and adds it to the canvas
    // at the specified position.
    addNodeToCanvas(node, x, y) {
        let key = node.id;
        let cNode = new CNode(x, y, node, this._workspace);
        this._nodeMap.set(key, cNode);
        this._workspace.appendChild(this._nodeMap.get(key).element);
        this._nodeMap.get(key).draw();
        this.applyZoomNode(cNode);
    }

    // Draws the connections (arrows) between outputs and inputs of nodes.
    drawConnections() {
        // clear canvas
        let ctx = this._canvas.getContext("2d");
        ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);

        for (let key of this._pipeline.nodeMap.keys()) {
            let node = this._pipeline.nodeMap.get(key);
            // console.log('Node ', node.id, node.inputs);
            if (helpers.isEmptyObject(node.inputs))
                continue;
            for (let ikey in node.inputs) {
                let input = node.inputs[ikey];
                if (input.source == undefined)
                    continue;

                // get 'to' coordinates
                let inputNodeComponent = this._nodeMap.get(node.id);
                let pTo = inputNodeComponent.getInputPos(input.name);

                // get 'from' coordinates
                let output = input.source;
                let outputNodeComponent = this._nodeMap.get(output.node.id);
                let pFrom = outputNodeComponent.getOutputPos(output.name);
                // console.log(`Node ${output.node.id} outputs ${output.name} to Node ${node.id} input ${input.name}`);
                // console.log(`Arrow (${pTo.x},${pTo.y}) to (${pFrom.x},${pFrom.y})`);
                this.drawArrow(pFrom.x, pFrom.y, pTo.x, pTo.y);
            }
        }
    }

    setNodePosition(id, x, y) {
        let node = this.nodeMap.get(id);
        node.x = x;
        node.y = y;
    }

    // Applies the current zoom to all nodes
    applyZoomNode(node) {
        let matrix = helpers.parseTransformMatrix(node.element.style.transform);
        let factor = 1 + this._zoomNumber * this._zoomStep;
        matrix[0] = factor;
        matrix[3] = factor;
        // translate nodes with respect to current zoom
        matrix[4] = node._unscaledX * matrix[0]; // x
        matrix[5] = node._unscaledY * matrix[3]; // y
        this.setNodePosition(node._id, matrix[4], matrix[5]);

        node.element.style.webkitTransform =
        node.element.style.transform = helpers.createTransformMatrixStr(matrix);
    }

    // Updates zoom of nodes and connections
    updateZoom() {
        for (let key of this.nodeMap.keys()) {
            let node = this.nodeMap.get(key);
            this.applyZoomNode(node);
        }
        // redraw connections with new node positions
        this.drawConnections();
    }

    zoomIn() {
        this._zoomNumber += 1;
        this.updateZoom();
    }

    zoomOut() {
        this._zoomNumber -= 1;
        this.updateZoom();
    }

    resetZoom() {
        this._zoomNumber = 0;
        this.updateZoom();
    }

    // bezier arrows
    drawArrow(fromx, fromy, tox, toy) {
        //variables to be used when creating the arrow
        let ctx = this._canvas.getContext("2d");

        // apply current scale
        ctx.setTransform(this._canvasTransform[0],
            this._canvasTransform[1],
            this._canvasTransform[2],
            this._canvasTransform[3],
            this._canvasTransform[4],
            this._canvasTransform[5]
        );

        // create two control points
        let midx = fromx + (tox - fromx) / 2;
        let midy = fromy + (toy - fromy) / 2;

        // TODO: make this more generic !!!
        let cp1x = fromx + 80;//midx;
        let cp1y = fromy;
        let cp2x = tox - 80;//midx;
        let cp2y = toy;

        // starting path of the arrow from the start square to the end square and drawing the stroke
        ctx.beginPath();
        ctx.moveTo(fromx, fromy);
        ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, tox, toy);
        ctx.strokeStyle = "#000";
        ctx.lineWidth = 1;
        ctx.stroke();
        ctx.closePath();

        let headlen = 10;
        let angle = Math.atan2(toy - cp2y, tox - cp2x);

        //starting a new path from the head of the arrow to one of the sides of the point
        ctx.beginPath();
        ctx.moveTo(tox, toy);
        ctx.lineTo(tox-headlen*Math.cos(angle-Math.PI/7),toy-headlen*Math.sin(angle-Math.PI/7));

        //path from the side point of the arrow, to the other side point
        ctx.lineTo(tox-headlen*Math.cos(angle+Math.PI/7),toy-headlen*Math.sin(angle+Math.PI/7));

        //path from the side point back to the tip of the arrow, and then again to the opposite side point
        ctx.lineTo(tox, toy);
        ctx.lineTo(tox-headlen*Math.cos(angle-Math.PI/7),toy-headlen*Math.sin(angle-Math.PI/7));

        //draws the paths created above
        ctx.strokeStyle = "#000";
        ctx.lineWidth = 2;
        ctx.stroke();
        ctx.fillStyle = "#000";
        ctx.fill();
        ctx.closePath();
    }

    // Returns the absolute node position from the layout of the node with
    // the given id.
    // If the node was not found, 'undefined' is returned
    getPosFromLayout(layout, nodeId) {
        let x = 0;
        let y = 0;
        let found = false;
        for (let idx in layout) {
            let obj = layout[idx];
            if (obj[nodeId] !== undefined) {
                x = obj[nodeId].x * this._workspace.offsetWidth;
                y = obj[nodeId].y * this._workspace.offsetHeight;
                found = true;
                break;
            }
        }
        let pos = {'x': x, 'y': y};
        if (found)
            return pos;
        return undefined;
    }

    // Serialize the current *unscaled* node positions. The position (x,y) of
    // each node will be normalized by the current workspace width/height so
    // that the resulting position is relative and in the range [0.0, 1.0].
    // Coordinates are fixed to 4 decimal places.
    serializeLayout() {
        let layout = [];
        for (let key of this.nodeMap.keys()) {
            let cNode = this.nodeMap.get(key);
            let obj = {};
            obj[cNode._node.id] = {
                'x': +((cNode._unscaledX / this._workspace.offsetWidth).toFixed(4)),
                'y': +((cNode._unscaledY / this._workspace.offsetHeight).toFixed(4))
            };
            layout.push(obj);
        }
        return layout;
    }

    get nodeMap() {
        return this._nodeMap;
    }

    get workspace() {
        return this._workspace;
    }

    get viewportWidth() {
        return this._workspaceViewport.offsetWidth;
    }

    get viewportHeight() {
        return this._workspaceViewport.offsetHeight;
    }

    // Remove the node (the dom element) with the given nodeId from the workspace
    removeNodeFromWorkspace (nodeId) {
        if (this._workspace.hasChildNodes()) {
            let children = this._workspace.childNodes;
            for (let i = 0; i < children.length; i++) {
                let child = children[i];
                let dataIndex = child.getAttribute('data-index');
                if (dataIndex == nodeId) {
                    this._workspace.removeChild(child);
                    break;
                }
            }
        }
    }

    // Removes the node with the given 'nodeId' from the worksace and deletes
    // its references in this._pipeline and this._nodeMap.
    // After the deletion, the connections are redrawn.
    handleEventDeleteNode(nodeId) {
        this.removeNodeFromWorkspace(nodeId);
        this._pipeline.deleteNode(nodeId);
        this._nodeMap.delete(nodeId);
        this.drawConnections();
    }

    // Adds a node to the editor given its node type.
    // The node is created with a radom id and added to the pipeline.
    // The canvas is then redrawn.
    handleEventAddNode(nodeTypeStr) {
        let nodeType = getNodeType(nodeTypeStr);
        let node = new nodeType({id: `${nodeTypeStr}-${helpers.getRandomNodeId()}`});
        this._pipeline.addNode(node);
        let x = Math.abs(this._leftTranslation) + 0.1 * this.viewportWidth;
        let y = Math.abs(this._topTranslation) + 0.1 * this.viewportHeight;
        this.addNodeToCanvas(node, x, y);
    }
}
