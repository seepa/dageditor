/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {isEmptyObject} from 'lib/helpers.js';

let nodeTypes = {};

export function getNodeType(nodeType) {
    return nodeTypes[nodeType];
}

export function registerNodeType (nType) {
    if (!(nType.prototype instanceof Node)) {
        throw 'Error: Node type must be subclass of Node!';
    }
    if (!(nType.type in nodeTypes))
        nodeTypes[nType.type] = nType;
}

// Serializes a node type to json
function serializeNodeType(nodeType) {
    // create a dummy instance so we can serialize it
    let dummyNode = new nodeType({id: 'dummy'});
    let nodeTypeObj = {'type': dummyNode.constructor.type};

    // serialize the inputs
    let inputs = {};
    Object.keys(dummyNode._inputs).forEach(function(key, index) {
        inputs[key] = dummyNode._inputs[key].toJson();
    });
    if (!isEmptyObject(inputs))
        nodeTypeObj['inputs'] = inputs;

    // serialize the outputs
    let outputs = {};
    Object.keys(dummyNode._outputs).forEach(function(key, index) {
        outputs[key] = dummyNode._outputs[key].toJson();
    });
    if (!isEmptyObject(outputs))
        nodeTypeObj['outputs'] = outputs;

    // serialize the options
    let options = {};
    Object.keys(dummyNode._options).forEach(function(key, index) {
        options[key] = dummyNode._options[key].toJson();
    });
    if (!isEmptyObject(options))
        nodeTypeObj['options'] = options;

    return nodeTypeObj;
}
// Serializes all registered node types to json
export function serializeNodeTypes() {
    let serialization = [];
    for(let nodeType in nodeTypes) {
        let s = serializeNodeType(nodeTypes[nodeType]);
        serialization.push(s);
    }
    return serialization;
}

export function pipelineFromJson(nodes) {
    if (typeof(nodes) == 'string') {
        console.log('pipelineFromJson: parsing stringified JSON input...');
        nodes = JSON.parse(nodes);
    }
    let nodeMap = new Map();
    // 1. create all nodes
    for (let i = 0; i < nodes.length; i++) {
        let jNode = nodes[i];
        if (!('type' in jNode))
            throw 'Error: Node is missing "type" attribute!';
        if (!('id' in jNode))
            throw 'Error: Node is missing "id" attribute!';
        if (nodeMap.has(jNode.id))
            throw 'Error: Duplicate Node id: ' + jNode.id;

        let nodeType = nodeTypes[jNode.type];
        if (nodeType == undefined)
            throw 'Error: Node type ' + jNode.type + ' is unkown (did you forget to register it?).'

        // create the node
        let node = new nodeType({id: jNode.id})
        // set the options
        for (let optName in jNode.options) {
            //console.log('ID: ' + jNode.id + ' optName: ' + optName + ' value: ' + jNode.options.value);
            node._options[optName].value = jNode.options[optName];
        }
        // add node to map
        nodeMap.set(node.id, node);
    }
    // 2. connect nodes
    for (let i = 0; i < nodes.length; i++) {
        let jNode = nodes[i];
        let node = nodeMap.get(jNode.id);
        for (let inputName in jNode.inputs) {
            let inputOpts = jNode.inputs[inputName];
            if (!('id' in inputOpts))
                throw 'Error: Input is missing an "id" attribute!';
            if (!('output' in inputOpts))
                throw 'Error: Input is missing an "output" attribute!';
            let outputNode = nodeMap.get(inputOpts.id);
            if (outputNode == undefined)
                throw 'Error: Node with ID ' + inputOpts.id + ' was sepcified as input but does not exist!';
            // console.log("Input node; ", node);
            // console.log("Output node; ", outputNode);
            node._inputs[inputName].source = outputNode._outputs[inputOpts.output];
        }
        // update node map
        nodeMap.set(jNode.id, node);
    }
    return new Pipeline(nodeMap);
}

function serializeNode(node) {
    let nodeObj = {
        'type': node.constructor.type,
        'id': node.id
    };
    // serialize the inputs
    let inputs = {};
    Object.keys(node._inputs).forEach(function(key, index) {
        if (node._inputs[key].source) {
            inputs[key] = {
                'id': node._inputs[key].source.node.id,
                'output': node._inputs[key].source.name
            }
        }
    });
    if (!isEmptyObject(inputs))
        nodeObj['inputs'] = inputs;
    // serialize the options
    let options = {};
    Object.keys(node._options).forEach(function(key, index) {
        // console.log(key, node._options[key], index);
        options[key] = node._options[key].value;
    });
    if (!isEmptyObject(options))
        nodeObj['options'] = options;

    return nodeObj;
}


export class Pipeline {
    constructor(nodeMap) {
        this._nodeMap = nodeMap;
    }

    get nodeMap() {
        return this._nodeMap;
    }

    getNode(id) {
        return this.nodeMap.get(id);
    }

    // Go through all nodes and check if their inputs still exist.
    // If an input does not exist anymore, it is set to 'undefined'.
    updateInputs() {
        for (let key of this.nodeMap.keys()) {
            let node = this.nodeMap.get(key);
            for (let ikey in node.inputs) {
                let input = node.inputs[ikey];
                if (input.source == undefined)
                    continue;
                let output = input.source;
                if (!this.nodeMap.has(output.node.id)) {
                    input.source = undefined;
                }
            }
        }
    }

    addNode(node) {
        this.nodeMap.set(node.id, node);
    }

    // Delete the node with the given id from the pipeline.
    // The inputs of the other nodes are updated after the deletion.
    deleteNode(id) {
        if (this.nodeMap.delete(id)) {
            this.updateInputs();
            return true;
        }
        return false;
    }

    toJson() {
        let pipelineObj = [];
        this.nodeMap.forEach(function(value, key, map){
            let nodeObj = serializeNode(value);
            pipelineObj.push(nodeObj);
        });
        return pipelineObj;
    }
}
