/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Add, Value} from 'lib/tests/test_node_types.js';

export function testDag() {
    console.log('Pipeline Test1: ');
    let v1 = new Value({id: 'v1', value: 2});
    let v2 = new Value({id: 'v2', value: 8});
    let add = new Add({id: 'Add1'});
    add._options['multiplier'].value = 4;
    add._inputs['v1'].source = v1._outputs['v']
    add._inputs['v2'].source = v2._outputs['v']
    console.log('Result of add: ' + add._outputs['v'].value + ' expected: ' + 40);
}
