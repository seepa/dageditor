/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict';

import {Node} from 'dag/node.js';
import {Number} from 'dag/typedvalue.js';

// define some node types which can be used during other tests
export class Value extends Node {
    constructor({value = undefined, id} = {}) {
        super({id});
        this._outputs = {
            'v' : new Number({title: 'Value'})
        }
        this._options = {
            'value' : new Number({title: 'Value', defaultValue: 0})
        }
        if (value != undefined) {
            this._options['value'].value = value;
        }
        this.init();
        console.log('Created node ', this);
    }
    static get type() {
        return 'Value';
    }
    compute() {
        this._outputs['v'].value = this._options['value'].value;
    }
}

export class Add extends Node {
    constructor(obj) {
        super(obj);
        this._outputs = {
            'v' : new Number({title: 'Sum'})
        }
        this._inputs = {
            'v1': new Number({title: 'Summand', defaultValue: 0}),
            'v2': new Number({title: 'Summand'})
        }
        this._options = {
            'multiplier' : new Number({title: 'Multiplier', defaultValue: 1})
        }
        this.init();
        console.log('Created node ', this);
    }
    static get type() {
        return 'Add';
    }
    compute() {
        this._outputs['v'].value = this._options['multiplier'].value *
            (this._inputs['v1'].value + this._inputs['v2'].value);
    }
}
