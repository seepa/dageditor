/*
 * Copyright (C) 2016, Patrick Seemann
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

'use strict'


import {Node} from 'dag/node.js';
import {TypedValue, Number, String} from 'dag/typedvalue.js';
import {Add, Value} from 'lib/tests/test_node_types.js';
import {Pipeline, registerNodeType, pipelineFromJson} from 'pipeline/pip.js';

registerNodeType(Value)
registerNodeType(Add)

let pipelineJson =
[
    {
        "type": "Value",
        "id": "v1",
        "options": {
            "value": 5
        }
    },
    {
        "type": "Value",
        "id": "v2",
        "options": {
            "value": 2
        }
    },
    {
        "type": "Add",
        "id": "add1",
        "inputs": {
            "v1": {
                "id": "v1",
                "output": "v"
            },
            "v2": {
                "id": "v2",
                "output": "v"
            }
        }
    },
    {
        "type": "Add",
        "id": "add2",
        "inputs": {
            "v1": {
                "id": "add1",
                "output": "v"
            }
        },
        "options": {
            "multiplier": 3
        }
    }
];

export function testSerialization1() {
    console.log('Test: Loading from Json: ');
    let pipeline = pipelineFromJson(pipelineJson);
    let resultNode = pipeline.getNode('add1');
    let resultNode2 = pipeline.getNode('add2');
    console.log('Result add1: ', resultNode._outputs['v'].value);
    console.log('Result add2: ', resultNode2._outputs['v'].value);

    console.log('Test: Serializing pipeline...');
    let pipelineStr = pipeline.toJson();
    console.log('Serialized Pipeline: ', pipelineStr);

    console.log('Test: parsing serialized pipeline...');
    let pipeline2 = pipelineFromJson(pipelineStr);
    let resultNode3 = pipeline2.getNode('add1');
    let resultNode4 = pipeline2.getNode('add2');
    console.log('Result add1: ', resultNode3._outputs['v'].value);
    console.log('Result add2: ', resultNode4._outputs['v'].value);

    // console.log(Pipeline.node_types_to_json());
}



//
// MVE pipeline example
//
import {Images} from '../node_types/images.js';
import {Makescene} from '../node_types/makescene.js';
import {Sfmrecon} from '../node_types/sfmrecon.js';

registerNodeType(Images)
registerNodeType(Makescene)
registerNodeType(Sfmrecon)

let reconPipelineJson = [
{
    "type" : "Images",
    "id": "img"
},
{
    "type": "Makescene",
    "id": "ms",
    "inputs": {
        "images-path": {
            "id": "img",
            "output": "images-path"
        }
    }
},
{
    "type" : "Sfmrecon",
    "id" : "sfm",
    "inputs" : {
        "scene-path": {
            "id": "ms",
            "output": "scene-path"
        }
    }
}
];

export function testSerialization2() {
    console.log('Test: loading reconstruction pipeline from json...');
    let reconPipeline = pipelineFromJson(reconPipelineJson);
    let sfm = reconPipeline.getNode('sfm');
    console.log('Recon pipeline result: ', sfm._outputs['scene-path'].value);
}
